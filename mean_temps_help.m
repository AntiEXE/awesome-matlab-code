clear all;
numGrades = 10; % Number of grades
grades = randi([0, 100], numGrades, 1); % Generates 'numGrades' random integers in the range [0, 100]

% Step 2: Write these grades to an Excel file named 'grades.xlsx'
filename = 'grades.xlsx';

writematrix(grades, filename); % If you're using MATLAB R2019a or newer
% xlswrite(filename, grades); % Use this if you're using an older version of MATLAB

fprintf('%d random grades have been written to %s\n', numGrades, filename);

x = xlsread('grades.xlsx'); % Read grades from Excel file

test = x<50;

% Count the number of grades in each category
fa = sum(x(x < 50)); % Counts grades less than 50


% Print variables to verify
fprintf('Fail (fa) count: %d\n', fa);
